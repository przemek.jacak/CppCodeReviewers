#include <cstdint> // for uint8_t
#include <vector>
#include <iostream>
#include <boost/iterator/iterator_facade.hpp>
#include <algorithm>
#include <boost/range/adaptor/transformed.hpp>

using namespace std;

class DataView {
public:
    DataView(uint8_t* aData, bool aFirstHalf) : mData(aData), mFirstHalf(aFirstHalf) {}

    bool flag()
    {
        return ((getData() & 0b1000u) != 0);
    }

    int data()
    {
        return getData() & 0b0111u;
    }

private:
    inline uint8_t getData()
    {
        uint8_t data = 0;

        if (mFirstHalf)
            data = (*mData) >> 4;
        else
            data = (*mData) & 0b00001111u;
        return data;
    }

    uint8_t* mData;
    bool mFirstHalf;
};

class BitsViewIterator
{
public:
    BitsViewIterator(uint8_t* aData, size_t aSize, size_t aPos) : mData(aData), mSize(aSize), mPos(aPos)
    {
    }

    auto operator!=(const BitsViewIterator& other)
    {
        if (this->mData != other.mData) return true;
        if (this->mSize != other.mSize) return true;
        if (this->mPos != other.mPos) return true;

        return false;
    }

    auto operator++()
    {
        if (mPos <= mSize)
            mPos++;

        return *this;
    }

    auto operator*()
    {
        if (mPos > mSize)
            throw out_of_range("Out of range");

        return DataView(mData + mPos/2, mPos%2 == 0);
    }

private:
    uint8_t* mData;
    size_t mSize;
    size_t mPos;
};

class BitsView
{
public:
    BitsView(uint8_t* aData, size_t aSize) : mData(aData), mSize(aSize)
    {
    }

    auto begin()
    {
        return BitsViewIterator(mData, mSize, 0);
    }

    auto end()
    {
        return BitsViewIterator(mData, mSize, mSize);
    }

private:
    uint8_t* mData;
    size_t mSize;
};

namespace std
{
template <> BitsViewIterator begin<BitsView>(BitsView& bv)
{
    return bv.begin();
}
template <> BitsViewIterator end<BitsView>(BitsView& bv)
{
    return bv.end();
}
}

auto decode(uint8_t* data, size_t size)
{
    return BitsView(data, size);
}

int main() {
    using namespace std;

    uint8_t data[] = { 0b10001001u, 0b10101100u, 0b00000001u, 0b00110111u };

    auto r = decode(data, 8);

    for (auto i : r)
    {
        cout << i.flag() << " " << i.data() << endl;
    }
}
